const Product = require('../models/Product');
const auth = require('../auth');

//get all product
	const getProducts = async (req, res) => {
		try {
			const products = await Product.find({});
			res.json(products);		
		} catch (error) {
			console.error(error);
			res.status(500).json({message: `Server error`});
		}
	};

//get product by id
	const getProductById = async (req, res) => {
		try {
			const product = await Product.findById(req.params.id);
			res.json(product);		
		} catch (error) {
			console.error(error);
			res.status(500).json({message: `Server error`});
		}
	};

//create product
	const createProduct = async (req, res) => {
		const data =  auth.decode(req.headers.authorization)
		if(data.isAdmin) {
			let newProduct = new Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
				countInStock: req.body.countInStock,
				imgUrl: req.body.imgUrl
			});
			return newProduct.save().then((product, err) => {
				const savedProduct = (err) ? false : product;
				res.json(savedProduct);
			})
		} else {
			res.json(false);
		}
	};

//update a product
	const updateProduct = (req, res) => {
		const data = {
			productId: req.params.productId,
			payload: auth.decode(req.headers.authorization),
			updatedProduct: req.body
		};

		return Product.findById(data.productId).then((result, err) => {
			if(data.payload.isAdmin) {
				result.name = data.updatedProduct.name;
				result.description = data.updatedProduct.description;
				result.price = data.updatedProduct.price;
				result.countInStock = data.updatedProduct.countInStock;
				return result.save().then((updatedProduct, err) => {
					const resultProduct = (err) ? false : true;
					res.json(resultProduct);
				})
			} else {
				res.json(false);
			}
		})
	};

//archive product
	const archiveProduct = async (req, res) => {
		const data = {
			productId: req.params.productId,
			payload: auth.decode(req.headers.authorization)
		}
		return Product.findById(data.productId).then((result, err) => {

			if(data.payload.isAdmin) {
				result.isActive = false;
				return result.save().then((updatedProduct, err) => {
					const resultProduct = (err) ? false : updatedProduct;
					res.json(resultProduct);
				})
			} else {
				res.json(false);
			}
		})
	};

// activate
	const activateProduct = async (req, res) => {
		const data = {
			productId: req.params.productId,
			payload: auth.decode(req.headers.authorization)
		}
		return Product.findById(data.productId).then((result, err) => {

			if(data.payload.isAdmin) {
				result.isActive = true;
				return result.save().then((updatedProduct, err) => {
					const resultProduct = (err) ? false : updatedProduct;
					res.json(resultProduct);
				})
			} else {
				res.json(false);
			}
		})
	};

//exports
	module.exports = {
		getProducts,
		getProductById,
		createProduct,
		updateProduct,
		archiveProduct,
		activateProduct
	};