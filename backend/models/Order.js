const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema({
  buyer: [
    {
      userId: {
        type: String,
      },
      _id: false,
    },
    {
      orderDate: {
        type: Date,
        default: new Date(),
      },
    },
  ],
  products: [
    {
      productId: {
        type: String,
      },
      quantity: {
        type: Number,
        default: 1,
      },
      _id: false,
    },
  ],
  amount: {
    type: Number,
  }
});

const CartSchema = new mongoose.Schema({
  items: [OrderSchema],
  subTotal: {
    default: 0,
    type: Number
  }
})

const Order = mongoose.model("Order", OrderSchema);
module.exports = Order;
