var mongoose = require("mongoose");

const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: [true, "Please enter a valid password"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
});

// UserSchema.plugin(uniqueValidator);
const userModel = mongoose.model("User", UserSchema);

userModel.createIndexes();
module.exports = mongoose.model("User", UserSchema);
