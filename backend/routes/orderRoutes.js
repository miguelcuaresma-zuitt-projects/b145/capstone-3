const express = require('express');
const router = express.Router();
const auth = require('../auth');

const {
	orderProduct,
	checkAllOrders,
	getBuyerOrders
} = require('../controller/orderControllers');

router.post('/', auth.verify, orderProduct);
router.get('/all', auth.verify, checkAllOrders);
router.get('/user-orders', auth.verify, getBuyerOrders);

module.exports = router;