const express = require('express');
const router = express.Router();
const auth = require('../auth');

const {
	registerUser,
	loginUser,
	getProfile,
	setAsAdmin,
	getAllUser
} = require('../controller/userControllers');

router.post('/registration', registerUser);
router.post('/login', loginUser)
router.post('/details', auth.verify, getProfile)
router.put('/:userId/set-admin', auth.verify, setAsAdmin);
router.get('/get-all', auth.verify, getAllUser)
module.exports = router;