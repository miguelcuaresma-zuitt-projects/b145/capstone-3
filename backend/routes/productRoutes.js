const express = require('express');
const router = express.Router();
const auth = require('../auth');

const {
	getProducts,
	getProductById,
	createProduct,
	updateProduct,
	archiveProduct,
	activateProduct
} = require('../controller/productControllers');

router.get('/', getProducts);
router.get('/:id', getProductById);
router.post('/create-new', auth.verify, createProduct);
router.put('/update/:productId', auth.verify, updateProduct);
router.put('/:productId/archive', auth.verify, archiveProduct);
router.put('/:productId/setActive', auth.verify, activateProduct);


module.exports = router;